// add click listener to all elements with countyclick class
// call showSAA and pass in element on click
document.querySelectorAll('.countyclick').forEach(county => county.addEventListener('click', function() {
    showSAA(county);
}));

// dict of county : SAA
const saaIndex = {
    'Kings': 'Kings SAA',
    'Kern': 'Kern SAA',
    'San_Bernardino': 'San Bernardino SAA'
};

// called when county is clicked
function showSAA(county) {
    //alert(county.getAttribute('id'));
    document.getElementById('saa').innerHTML = '<h2>' + saaIndex[county.getAttribute('id')] + '</h2>';
}